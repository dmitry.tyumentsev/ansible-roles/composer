Composer Role
=========

This role installs composer on Ubuntu/Debian server.

Example Playbook
=========
```yml
- hosts: all
  become: true
  roles:
    - composer
  vars:
    php_version: 7.4
```
